package main

import (
	"net/http"
	"os"

	"bitbucket.org/johnmunyui/hackdose/app/routes"
	"github.com/gorilla/handlers"
)

func main() {
	router := routes.Routes()
	http.ListenAndServe("localhost:3000", handlers.LoggingHandler(os.Stdout, router))
}
