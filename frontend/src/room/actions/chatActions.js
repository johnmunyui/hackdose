import 'whatwg-fetch'

import {
    SEND_MESSAGE,
    CONNECT,
    ADD_MESSAGE,
    FETCH_TODAYS_MESSAGES,
    ADD_TODAYS_MESSAGES,
    UPDATE_ROOM_STATUS
} from '../constants'

import {
    getToken,
    getRoom
} from '../api/urlSplit'


const fetchMessagesPromise = () => {
    let room = getRoom()
    let token = getToken()

    return fetch(`/chats/retrieve_todays_messages/${room}/`, {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Token ${token}`
        }
    })
}


export const fetchTodaysMessages = () => {
    return (dispatch) => {
        fetchMessagesPromise().then((response) => {
            return response.json()
        }).then((json) => {
            dispatch(addTodaysMessages(json))
        }).catch((err) => {
            console.log("parsing failed", err)
        })
    }
}

export const connect = (room, token) => {
    return {
        type: CONNECT,
        room,
        token
    }
}

export const sendMessage = (message) => {
    return {
        type: SEND_MESSAGE,
        message: message
    }
}

export const addMessage = (message) => {
    return {
        type: ADD_MESSAGE,
        message: message
    }
}

export const updateRoomStatus = (data) => {
    return {
        type: UPDATE_ROOM_STATUS,
        users: data.members,
        room: data.room
    }
}

export const addTodaysMessages = (messages) => {
    return {
        type: ADD_TODAYS_MESSAGES,
        messages
    }
}
