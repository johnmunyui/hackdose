import 'whatwg-fetch'
import {
    FETCH_TASKS,
    ADD_TASKS
} from '../constants'

import {
    getToken,
    getRoom
} from '../api/urlSplit'

const fetchTasksPromise = () => {
    let room = getRoom()
    let token = getToken()
    
    return fetch(`/rooms/task_list/${room}/`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Token ${token}`
        }
    })
}

export const fetchTasks = () => {
    return (dispatch) => {
      fetchTasksPromise().then((response) => {
            return response.json()
        }).then((json) => {
            dispatch(addTasks(json))
        }).catch((ex) => {
            console.log("parsing failed", ex)
        })
    }
}

export const addTasks = (tasks) => {
    return {
        type: ADD_TASKS,
        tasks: tasks
    }
}
