import {
    createStore,
    applyMiddleware,
    combineReducers
} from 'redux'
import thunk from 'redux-thunk'
import {
    socketMiddleware
} from '../middleware'

import {
    chatReducer,
    taskReducer
} from '../reducers'


export const configureStore = () => {
    const reducers = combineReducers({
        chat: chatReducer,
        task: taskReducer,
    })

    return createStore(
        reducers,
        applyMiddleware(thunk, socketMiddleware)
    )
}
