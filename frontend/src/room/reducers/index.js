import {
    chatReducer
} from './chatReducer'

import {
    taskReducer
} from './taskReducer'

export {
    chatReducer,
    taskReducer
}
