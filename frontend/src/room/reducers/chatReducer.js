import {
    ADD_MESSAGE,
    UPDATE_ROOM_STATUS,
    ADD_TODAYS_MESSAGES
} from '../constants'

import { spawnNotificaction } from '../api/notification'

const initialState = {
    messages: [],
    users: [],
    room: "",
}

export const chatReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_MESSAGE: 
            var newState = {...state,
                messages: [...state.messages, action.message]
            }
            spawnNotificaction(state.room, action.message.sender)
            return newState
        case UPDATE_ROOM_STATUS:
            var newState = {...state,
                users: action.users,
                room: action.room
            }
            return newState
        case ADD_TODAYS_MESSAGES:
            var newState = {...state,
                messages: action.messages
            }
            return newState
        default:
            return state
    }
}

const setRoomName = (room) => {
    let roomName = room.replace("+", " ")
    return roomName
}
