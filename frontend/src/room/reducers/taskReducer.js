import {
    ADD_TASKS
} from '../constants'

const initialState = {
    tasks: [],
}

export const taskReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TASKS:
            var newState = {...state,
                tasks: action.tasks
            }

            return newState
        default:
            return state
    }
}
