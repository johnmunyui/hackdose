import {
    addMessage,
    updateRoomStatus
} from '../actions'

export const socketMiddleware = (() => {
    var socket = null;

    const onOpen = (ws, store, token) => evt => {
        //Send a handshake, or authenticate with remote end

        //Tell the store we're connected
        // store.dispatch(actions.connected());
        // ws.send("wow")
    }

    const onClose = (ws, store) => evt => {
        //Tell the store we've disconnected
        // store.dispatch(actions.disconnected());
    }

    const onMessage = (ws, store) => evt => {
        // Parse the JSON message received on the websocket

        let msg = JSON.parse(evt.data) 
        switch (msg.type) {
            case "CHAT_MESSAGE":
                // Dispatch an action that adds the received message to our state
                store.dispatch(addMessage(msg));
                break
            case "ROOM_STATUS":
                store.dispatch(updateRoomStatus(msg))
                break
            default:
                console.log("Received unknown message type: '" + msg.type + "'")
                break;
        }
    }

    return store => next => action => {
        switch (action.type) {

            //The user wants us to connect
            case 'CONNECT':
                //Start a new connection to the server
                if (socket != null) {
                    socket.close()
                }
                //Send an action that shows a "connecting..." status for now
                // store.dispatch(actions.connecting());

                //Attempt to connect (we could send a 'failed' action on error)
                let url = `ws://localhost:3000/chat/${action.room}/${action.token}/`
                socket = new WebSocket(url);
                socket.onmessage = onMessage(socket, store)
                socket.onclose = onClose(socket, store)
                socket.onopen = onOpen(socket, store)
                break;

                //The user wants us to disconnect
            case 'DISCONNECT':
                if (socket != null) {
                    socket.close()
                }
                socket = null

                //Set our state to disconnected
                // store.dispatch(actions.disconnected());
                break

                //Send the 'SEND_MESSAGE' action down the websocket to the server
            case 'SEND_MESSAGE':
                socket.send(action.message)
                break

                //This action is irrelevant to us, pass it on to the next middleware
            default:
                return next(action)
        }
    }

})();
