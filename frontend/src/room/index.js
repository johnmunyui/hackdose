import {
    render
} from 'react-dom'
import React from 'react'
import {
    Provider
} from 'react-redux'
import 'materialize-css/dist/css/materialize.min.css';

// Create store
import {
    configureStore
} from './store'

// Container component
import Room from './components'

const root = document.getElementById('room')
const store = configureStore()

render(
    <Provider store={store}>
        <Room />
    </Provider>,
    root
)
