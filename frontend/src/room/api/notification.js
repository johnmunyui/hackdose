/*
 * This module holds useful apis for browser notifications
 */


export const configureNotificationPermission = () => {
    // Check if browser supports Notifications
    if (!("Notification" in window)) {
        alert("This browser does not support desktop notification");
    }

    // Check if notifications permission have already been granted.
    else if (Notification.permission === "granted") {
        // If it's okay let's create a notification
        let options = {
            icon: "/static/img/icons/logo.png",
            body: "Hey there, Hack on"
        }
        let notification = new Notification("Clutchdevs", options);
    
        // close notification after 3 seconds
        setTimeout(() => {
            notification.close()
        }, 3000);
    }

    // Ask for user permission it is not granted
    else if (Notification.permission !== "denied") {
        Notification.requestPermission(function(permission) {
            // If the user accepts, let's create a notification
            if (permission === "granted") {
                var notification = new Notification("Hi there!");
            }
        });
    }
}

export const spawnNotificaction = ( room, sender ) => {
    var options = {
        body: `New message from ${sender}`,
        icon: "/static/img/icons/logo.png"
    }

    var notification = new Notification(room, options)

    setTimeout(() => {
            notification.close()
        }, 3000); 
}
