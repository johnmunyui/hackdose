/*
 * This module handles what ui should be deisplayed based on 
 * user clicks. Eg user clicks chat link, chat ui is displayed and
 * so on.
 * 
 */

export const renderUI = (divId) => {
    let uis = $("#uis").children()

    uis.each(function() {
        if (this.id.startsWith(divId)) {
            $(this).css('display', 'block')
        } else {
            $(this).css('display', 'none')
        }
    });
}
