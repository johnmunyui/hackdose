export default class ChatWS {
    constructor(room, dispatcher) {
        let url = new WebSocket((`ws://localhost:3000/chat/${room}/`))
        this.dispatcher = dispatcher
        this.websocket = new WebSocket(url)

        this.websocket.onmessage = (event) => {
            dispatcher(event.data)
        }
    }
    postMessage(message) {
        this.websocket.send(
            message
        )
    }

    close() {
        this.websocket.close();
    }
}
