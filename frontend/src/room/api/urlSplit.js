/*
 * This module grabs important information from the url
 * Information includes the group the user belongs to
 * and the authentication token
 */

const location = window.location.pathname;
export const getToken = () => {
    let splitedUrl = location.split("/")
    return splitedUrl[4]
}

export const getRoom = () => {
    let splitedUrl = location.split("/")
    return splitedUrl[3]
}
