/*
 * The following module holds action types for the chat ui.
 * Action types include adding a message, adding a user and
 * deleting a user
 */

// connect to the websocket
export const CONNECT = 'CONNECT'

// send message to server
export const SEND_MESSAGE = 'SEND_MESSAGE'

// recieve message from the server
export const RECIEVE_MESSAGE = 'RECIEVE_MESSAGE'

// adds and removes users from rooms online users
// information is recieved from the server
export const UPDATE_ROOM_STATUS = 'UPDATE_ROOM_STATUS'

// add message to the messages array
export const ADD_MESSAGE = 'ADD_MESSAGE'

// fetch today's messages
export const FETCH_TODAYS_MESSAGES = 'FETCH_TODAYS_MESSAGES'

// add today's messages to state
export const ADD_TODAYS_MESSAGES = 'ADD_TODAYS_MESSAGES'
