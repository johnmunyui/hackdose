/*
 * This module holds all chat constants
 *
 */


// fetch tasks
export const FETCH_TASKS = 'FETCH_TASKS'

// add tasks to state
export const ADD_TASKS = 'ADD_TASKS'
