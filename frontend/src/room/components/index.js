require('materialize-css/dist/js/materialize.js');
require('materialize-css/js/init.js');


import React from 'react'
import {
    connect
} from 'react-redux'


import ChatBox from './chat'
import TaskBox from './tasks'
import Navigation from './navigation'
import {
    configureNotificationPermission
} from '../api/notification'

class Room extends React.Component {
    constructor() {
        super();
    }

    componentDidMount() {
        configureNotificationPermission()

    }
    render() {
        const {
            dispatch
        } = this.props
        const {
            room
        } = this.props
        const {
            messages
        } = this.props

        return (
            <div>
                <Navigation room={ this.props.room } users={ this.props.users } />  
                <div id="uis">
                    <ChatBox dispatch={ dispatch } messages={ this.props.messages } />
                    <TaskBox dispatch={ dispatch } tasks={ this.props.tasks } />
                </div>
            </div>
        )
    }
}


const stateToProps = (state) => {

    return {
        messages: state.chat.messages,
        users: state.chat.users,
        room: state.chat.room,
        tasks: state.task.tasks
    }
}

export default connect(stateToProps)(Room)
