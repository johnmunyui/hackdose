import 'whatwg-fetch'
import React from 'react'
import moment from 'moment'

import {
    fetchTasks
} from '../../actions'

import {
    getToken
} from '../../api/urlSplit'

import PreLoader from './preLoader'

class IncompleteTasks extends React.Component {
    constructor() {
        super()
    }

    render() {
        const {
            dispatch
        } = this.props
        const incompleteTaskNodes = this.props.tasks.map((task) => {
            return (
                <div className="card-panel" key={ task.id }>
                    <IncompleteTask task={ task } />
                </div>
            )
        })
        return (
            <div id="incomplete_tasks" style={{ marginTop: "25px" }} className="col s12">
                { incompleteTaskNodes }
                <ConfirmationModal dispatch={ dispatch }/>
            </div>
        )
    }
}

class IncompleteTask extends React.Component {
    constructor() {
        super()
    }

    render() {
        let task = this.props.task
        var isPicked = false
        if (task.status == "not_picked") {
            isPicked = false
        } else {
            isPicked = true
        }
        return (
            <div className="row">
                <div className="col m12">
                    <div className="row">
                        <table className="responsive-table">
                            <thead>
                                <tr>
                                    <th> Task </th>
                                    <th> Added by </th>
                                    <th> Date added </th>
                                    <th> Date Picked </th>
                                    <th> Duration </th>
                                    <th> Picked By </th>
                                    <th> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td> { task.name } </td>
                                    <td>{ task.added_by }</td>
                                    <td>{ moment(task.date_added).fromNow() }</td>
                                    <td>
                                        {isPicked ? (
                                             moment(task.date_taken).fromNow()
                                        ) : (
                                            "Not taken"
                                        )}
                                        </td>
                                    <td>{ task.duration } Hours </td>
                                    <td> { task.taken_by } </td>
                                    <td>
                                        {isPicked ? (
                                            <CompleteTaskBtn taskName={ task.name } taskId={ task.id } />
                                        ) : (
                                            <PickTaskBtn taskName={ task.name } taskId={ task.id } duration={ task.duration } />
                                        )}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

class PickTaskBtn extends React.Component {
    constructor() {
        super()

        this.openConfirmationModal = this.openConfirmationModal.bind(this)
    }

    openConfirmationModal() {
        let modal = $("#confirmation_modal")

        modal.find("h5").text(`Are you sure you want to pick "${ this.props.taskName }" ?`)
        modal.find("p").html(`Duration <b> ${ this.props.duration } Hours</b>`)
        modal.find("h6").text(this.props.taskId)
        modal.find("span").text("pick_task")
        modal.modal('open')
    }
    render() {
        return (
            <button className="waves-effect waves-light btn grey darken-4" onClick={this.openConfirmationModal}>
                <i className="material-icons left">shopping_basket</i>
                    Pick task
            </button>
        )
    }
}

class CompleteTaskBtn extends React.Component {
    constructor() {
        super()
        this.openConfirmationModal = this.openConfirmationModal.bind(this)
    }
    openConfirmationModal() {
        let modal = $("#confirmation_modal")

        modal.find("h5").text(`Are you sure you want to mark task "${ this.props.taskName }" as completed ?`)
        modal.find("h6").text(this.props.taskId)
        modal.find("span").text("complete_task")
        modal.find("p").text("")
        $("#confirm_complete_task").removeClass("disabled")
        $("#complete-task-err").addClass("hide")
        modal.modal('open')

    }
    render() {
        return (
            <button className="waves-effect waves-light btn grey darken-4" onClick={this.openConfirmationModal}>
                <i className="material-icons left">done_all</i>
                    Complete ? 
            </button>
        )
    }
}

class ConfirmationModal extends React.Component {
    constructor() {
        super()

        this.updateTask = this.updateTask.bind(this)
        this.pickTask = this.pickTask.bind(this)
        this.completeTask = this.completeTask.bind(this)
        this.state = {
            isLoading: false
        }
    }
    updateTask(ev) {
        let updateType = $("#update_type").text()
        switch (updateType) {
            case "pick_task":
                this.pickTask()
                break
            case "complete_task":
                this.completeTask()
                break
            default:
                console.log("men")
        }
    }
    pickTask() {
        this.setState({
            isLoading: true
        })

        let taskIdentity = $("#incomplete_task_picked").html()
        let token = getToken()
        let modal = $("#confirmation_modal")
        const dispatch = this.props.dispatch

        fetch(`/rooms/update_task/${taskIdentity}/pick_task/`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${token}`
            },
            body: JSON.stringify({
                status: "picked"
            })
        }).then((response) => {
            return response.status
        }).then((status) => {
            dispatch(fetchTasks())
            this.setState({
                isLoading: false
            })
            modal.modal('close');
        }).catch((err) => {

        })
    }
    completeTask() {
        this.setState({
            isLoading: true
        })

        let taskIdentity = $("#incomplete_task_picked").html()
        let token = getToken()
        let modal = $("#confirmation_modal")
        const dispatch = this.props.dispatch

        fetch(`/rooms/update_task/${taskIdentity}/task_complete/`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${token}`
            },
            body: JSON.stringify({
                status: "picked"
            })
        }).then((response) => {
            return response
        }).then((response) => {
            if (response.status != 204) {
                var error = new Error(response.statusText)
                error.response = response
                throw error
            }
        }).then((response) => {
            dispatch(fetchTasks())
            this.setState({
                isLoading: false
            })
            modal.modal('close');
        }).catch((err) => {
            this.setState({
                isLoading: false
            })
            $("#confirm_complete_task").addClass("disabled")
            $("#complete-task-err").removeClass("hide")
        })
    }
    render() {
        var isLoading = this.state.isLoading
        return (
            <div id="confirmation_modal" className="modal">
                <h6 className="hide" id="incomplete_task_picked"></h6>
                <span className="hide" id="update_type"></span>
                <div className="modal-content">
                    <h5></h5>
                    <p></p>
                    <strong className="red-text hide" id="complete-task-err">
                        I'm sorry, this was not your task
                        <i className="material-icons left">error_outline</i>
                    </strong>
                </div>
                <div className="modal-footer">
                    { isLoading ? (
                        <PreLoader /> 
                    ) : ( 
                        <ModalFooterButtons onUpdateTask={ this.updateTask }/>
                    )}
                </div>
            </div>
        )
    }
}

class ModalFooterButtons extends React.Component {
    constructor() {
        super()
        this.closeModal = this.closeModal.bind(this)
    }
    closeModal(ev) {
        $("#confirmation_modal").modal('close');
    }
    render() {
        return (
            <div>
                <button id="confirm_complete_task" className="btn-flat waves-green waves-effect waves-green" onClick={ this.props.onUpdateTask }>
                    <i className="material-icons left">done</i>
                    yes
                </button>
                <button className="btn-flat waves-green waves-effect waves-green" onClick={ this.closeModal }>
                    <i className="material-icons left">cancel</i>
                    No
                </button>
            </div>
        )
    }
}

export default IncompleteTasks
