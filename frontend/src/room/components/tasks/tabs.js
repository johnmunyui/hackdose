import React from 'react'
import NewTasks from './newTasks'
import IncompleteTasks from './incompleteTasks'
import CompleteTasks from './completeTasks'
import TestedTasks from './testedTasks'
import AddTasks from './addTasks'

class TaskTabs extends React.Component {
    constructor() {
        super()
    }

    render() {
        let groupedTasks = groupTasks(this.props.tasks)
        var roomId = null
        if (this.props.tasks.length > 0) {
            roomId = this.props.tasks[0].roomid
        }

        return (
            <div className="row">
                <div className="col s12">
                    <ul className="tabs">
                        <li className="tab col s2"><a href="#new_tasks" className="active">New</a></li>
                        <li className="tab col s2"><a href="#incomplete_tasks">Incomplete</a></li>
                        <li className="tab col s2"><a href="#complete_tasks">Complete</a></li>
                        <li className="tab col s2"><a href="#tested_tasks">Tested</a></li>
                        <li className="tab col s2"><a href="#add_new_tasks">Add</a></li>
                    </ul>
                </div>
                <NewTasks tasks={ groupedTasks.new } dispatch={ this.props.dispatch }  />
                <IncompleteTasks tasks={ groupedTasks.incomplete } dispatch={ this.props.dispatch }/>
                <CompleteTasks tasks={ groupedTasks.complete } dispatch={ this.props.dispatch } />
                <TestedTasks tasks={ groupedTasks.tested } dispatch={ this.props.dispatch }/>
                <AddTasks dispatch={ this.props.dispatch } roomId = { roomId } />
            </div>
        )
    }
}

const groupTasks = (tasks) => {
    let groupedTasks = {
        "new": [],
        "incomplete": [],
        "complete": [],
        "tested": []
    }

    if (tasks.length == 0) {
        return groupedTasks
    }

    for (var i=0; i<tasks.length; i++) {
        switch (tasks[i].status) {
            case "new":
                groupedTasks.new.push(tasks[i])
                break
            case "not_picked":
                groupedTasks.incomplete.push(tasks[i])
                break
            case "picked":
                groupedTasks.incomplete.push(tasks[i])
                break
            case "completed":
                groupedTasks.complete.push(tasks[i])
                break
            case "tested":
                groupedTasks.tested.push(tasks[i])
                break
            default:
                continue
        }
    }

    return groupedTasks
}

export default TaskTabs
