import 'whatwg-fetch'
import React from 'react'
import {
    fetchTasks
} from '../../actions'

import {
    getToken
} from '../../api/urlSplit'


class AddTasks extends React.Component {
    constructor() {
        super()

        this.state = {
            tasks: [],
        }

        this.addNewTask = this.addNewTask.bind(this)
        this.removeTask = this.removeTask.bind(this)
    }

    addNewTask(task) {
        let tasks = this.state.tasks
        tasks.push(task)

        this.setState({
            "tasks": tasks
        })
    }

    removeTask(index) {
        let tasks = this.state.tasks
        tasks.splice(index, 1)

        this.setState({
            "tasks": tasks
        })
    }
    render() {
        const {
            dispatch
        } = this.props
        return (
            <div id="add_new_tasks" style={{ marginTop: "25px" }} className="col s12 m12">
                <div className="card">
                    <div className="card-content">
                        <NewTaskList tasks={ this.state.tasks } onRemoveTask={ this.removeTask } />  
                    </div>
                    <div className="card-action">
                        <TaskInput onAddNewTask={ this.addNewTask } roomId = { this.props.roomId } dispatch ={ dispatch } />
                    </div>
                </div>
            </div>
        )
    }
}

class NewTaskList extends React.Component {
    constructor() {
        super()

        this.removeTask = this.removeTask.bind(this)
    }
    removeTask(ev) {
        let index = $(ev.target).parent().data("key")

        this.props.onRemoveTask(index)

    }
    render() {
        const tasks = this.props.tasks
        const newTaskNodes = tasks.map((task, index) => {
            return (
                <a href="javascript:void(0)" key={index} className="collection-item" data-key={index}>
                    { task }
                    <i className="material-icons prefix right" onClick={ this.removeTask }>delete</i>
                </a>
            )
        })
        return (
            <div className="collection" id="new_tasks_collection">
                { newTaskNodes } 
            </div>
        )
    }
}

class TaskInput extends React.Component {
    constructor() {
        super()

        this.handleReturnPressed = this.handleReturnPressed.bind(this)
    }

    handleReturnPressed(ev) {
        var event = ev || window.event;
        var charCode = event.which || event.keyCode;

        var newTask = event.target.value

        if (!newTask) {
            return
        }

        if (charCode == '13') {

            this.props.onAddNewTask(newTask)
            ev.target.value = ""
        }
    }
    render() {
        return (
            <div>
                <div className="input-field">
                    <input id="new_task" type="text" className="validate" onKeyPress={ this.handleReturnPressed }></input>
                    <label htmlFor="new_task">Task</label>
                </div>
                <CommitBtn roomId ={ this.props.roomId } dispatch = { this.props.dispatch } />
            </div>
        )
    }
}

class CommitBtn extends React.Component {
    constructor() {
        super()

        this.postNewTasks = this.postNewTasks.bind(this)
    }

    postNewTasks() {
        let taskDiv = $("#new_tasks_collection")
        let taskNodes = taskDiv.find("a")
        let roomId = this.props.roomId
        const {
            dispatch
        } = this.props
        let token = getToken()
        let tasksToPost = []

        for (var taskNode of taskNodes) {
            tasksToPost.push($(taskNode).children().remove().end().text().trim())
            $(taskNode).remove()
        }

        var sendData = JSON.stringify(tasksToPost)
        fetch(`/rooms/add_more_tasks/${roomId}/`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${token}`
            },
            body: sendData
        }).then((response) => {
            return response.status
        }).then((status) => {
            dispatch(fetchTasks())
        }).catch((err) => {
            console.log(err)
        }).catch((err) => {
            console.log(err)
        })
    }
    render() {
        return (
            <button className="waves-effect waves-light btn grey darken-4" onClick={ this.postNewTasks }>
                <i className="material-icons right">save</i>
                Commit
            </button>
        )
    }
}

export default AddTasks
