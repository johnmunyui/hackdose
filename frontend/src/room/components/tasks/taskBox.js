import React from 'react'
import {
    fetchTasks
} from '../../actions'
import TaskList from './taskList'
import TaskTabs from './tabs'


class TaskBox extends React.Component {
    constructor() {
        super();
    }
    componentDidMount() {
        const {
            dispatch
        } = this.props

        dispatch(fetchTasks())

        setInterval(() => { 
            if (window.location.pathname.startsWith("/rooms/hackon/")) {
                dispatch(fetchTasks())
            }
        }, 30000)
    }
    render() {
        const style = {
            "display": "none"
        }
        var tasks = this.props.tasks
        return (
            <main id="task-main" style={style}>
                <TaskTabs tasks={ tasks } dispatch={ this.props.dispatch }/> 
            </main>
        )
    }
}

export default TaskBox
