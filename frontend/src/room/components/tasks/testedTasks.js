import React from 'react'
import moment from 'moment'

class TestedTasks extends React.Component {
    constructor() {
        super()
    }

    render() {
        const testedTaskNodes = this.props.tasks.map((task) => {
            return (    
                <div className="card-panel" key={ task.id }>
                    <TestedTask task={ task } />
                </div>
            )
        })
        return (
            <div id="tested_tasks" style={{ marginTop: "25px" }} className="col s12">
                { testedTaskNodes }
            </div>
        )
    }
}

class TestedTask extends React.Component {
    constructor() {
        super()
    }

    render() {
        let task = this.props.task 
        return (
            <div className="row">
                <div className="col m12">
                    <div className="row">
                        <table className="responsive-table">
                            <thead>
                                <tr>
                                    <th> Task </th>
                                    <th> Added by </th>
                                    <th> Date added </th>
                                    <th> Date Picked </th>
                                    <th> Duration </th>
                                    <th> Picked By </th>
                                    <th> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td> { task.name } </td>
                                    <td>{ task.added_by }</td>
                                    <td>{ moment(task.date_added).fromNow() }</td>
                                    <td>{ moment(task.date_taken).fromNow() }</td>
                                    <td>{ task.duration } Hours </td>
                                    <td> { task.taken_by } </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default TestedTasks
