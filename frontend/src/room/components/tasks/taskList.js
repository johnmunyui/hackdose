import React from 'react'

class TaskList extends React.Component {
    constructor() {
        super()
    }

    render() {
        const style = {
            "display": "none"
        }
        console.log(this.props.tasks)
        var taskNodes = this.props.tasks.tasks.map((task) => {
            return (
                <div className="card-panel" key={ task.id }>
                    <span className="blue-text text-darken-2">{ task.name } </span>
                </div>

            )
        })
        return (
            <div className="row">
                <h4> Incomplete tasks </h4>
                { taskNodes } 
            </div>
        )
    }
}

export default TaskList
