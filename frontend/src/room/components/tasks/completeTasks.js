import React from 'react'
import moment from 'moment'

import {
    fetchTasks
} from '../../actions'

import {
    getToken
} from '../../api/urlSplit'

import PreLoader from './preLoader'

class CompleteTasks extends React.Component {
    constructor() {
        super()
    }

    render() {
        const { dispatch } = this.props
        const completeTaskNodes = this.props.tasks.map((task) => {
            return (    
                <div className="card-panel" key={ task.id }>
                    <CompleteTask task={ task } />
                </div>
            )
        })
        return ( 
            <div id="complete_tasks" style={{ marginTop: "25px" }} className="col s12">
                { completeTaskNodes }

                <TaskTestedModal dispatch={ dispatch } />
            </div>
        )
    }
}

class CompleteTask extends React.Component {
    constructor() {
        super()
    }

    render() {
        let task = this.props.task 
        return (
            <div className="row">
                <div className="col m12">
                    <div className="row">
                        <table className="responsive-table">
                            <thead>
                                <tr>
                                    <th> Task </th>
                                    <th> Added by </th>
                                    <th> Date added </th>
                                    <th> Date Picked </th>
                                    <th> Duration </th>
                                    <th> Picked By </th>
                                    <th> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td> { task.name } </td>
                                    <td>{ task.added_by }</td>
                                    <td>{ moment(task.date_added).fromNow() }</td>
                                    <td>{ moment(task.date_taken).fromNow() }</td>
                                    <td>{ task.duration } Hours </td>
                                    <td> { task.taken_by } </td>
                                    <td>
                                        <MarkTaskAsTestedBtn taskName={ task.name } taskId={ task.id }/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

class MarkTaskAsTestedBtn extends React.Component {
    constructor() {
        super()

        this.taskTestedModal = this.taskTestedModal.bind(this)
    }
    taskTestedModal(ev) {
        let modal = $("#task_tested")
        modal.find("h5").text(`Are you sure you want to mark task "${this.props.taskName}" as tested?`)
        modal.find("h6").text(this.props.taskId)
        modal.modal('open')
    }

    render() {
        return (
            <button className="waves-effect waves-light btn grey darken-4" onClick={this.taskTestedModal}>
                <i className="material-icons left">gavel</i>
                    Tested? 
            </button>
        )
    }
}

class TaskTestedModal extends React.Component {
    constructor() {
        super()

        this.updateTask = this.updateTask.bind(this)
        this.state = {
            isLoading: false
        }
    }
    updateTask(ev) {
        this.setState({
            isLoading: true
        })

        let taskIdentity = $("#task_tested_id").text()
        console.log(taskIdentity)
        let token = getToken()
        let modal = $("#task_tested")
        const dispatch = this.props.dispatch

        fetch(`/rooms/update_task/${taskIdentity}/task_tested/`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${token}`
            },
            body: JSON.stringify({
                status: "tested",
            })
        }).then((response) => {
            return response.status
        }).then((status) => {
            dispatch(fetchTasks())
            this.setState({
                isLoading: false
            })
            modal.modal('close');
        }).catch((err) => {
            console.log(err)
        }).catch((err) => {
            console.log(err)
        })
    }
    render() {
        var isLoading = this.state.isLoading
        return (
            <div id="task_tested" className="modal">
                <h6 className="hide" id="task_tested_id"></h6>
                <div className="modal-content">
                    <h5 id="task_name"></h5>
                    <p> </p> 
                </div>
                <div className="modal-footer">
                    { isLoading ? (
                        <PreLoader />
                    ) : (
                        <ModalFooterButtons onUpdateTask={ this.updateTask }/>
                    )}
                </div>
            </div>
        )
    }
}

class ModalFooterButtons extends React.Component {
    constructor() {
        super()
    }

    render() {
        return (
            <div>
                <button className="btn-flat waves-green waves-effect waves-green" onClick={ this.props.onUpdateTask }>
                    <i className="material-icons left">done</i>
                    yes
                </button>
                <button className="btn-flat waves-green waves-effect waves-green modal-action modal-close">
                    <i className="material-icons left">cancel</i>
                    No
                </button>
            </div>
        )
    }
}

export default CompleteTasks
