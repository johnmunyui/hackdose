import 'whatwg-fetch'
import React from 'react'
import moment from 'moment'

import {
    fetchTasks
} from '../../actions'

import {
    getToken
} from '../../api/urlSplit'

import PreLoader from './preLoader'

class NewTasks extends React.Component {
    constructor() {
        super()
    }

    render() {
        const {
            dispatch
        } = this.props
        const newTaskNodes = this.props.tasks.map((task) => {
            return (
                <div className="card-panel" key={ task.id }>
                    <NewTask task={ task } />
                </div>
            )
        })

        return (
            <div id="new_tasks" style={{ marginTop: "25px" }}  className="col s12">
                { newTaskNodes }
                <AddDurationModal dispatch={ dispatch }/>
            </div>
        )
    }
}

class NewTask extends React.Component {
    constructor() {
        super()
    }

    render() {
        let task = this.props.task
        return (
            <div className="row">
                <div className="col m12">
                    <div className="row">
                        <table>
                            <thead>
                                <tr>
                                    <th> Task </th>
                                    <th> Added by </th>
                                    <th> Date added </th>
                                    <th> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr> 
                                    <td> { task.name } </td>
                                    <td>{ task.added_by }</td>
                                    <td>{ moment(task.date_added).fromNow() }</td>
                                    <td><AddDurationBtn taskId = { task.id } taskName = { task.name }/> </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div> 
            </div>
        )
    }
}

class AddDurationBtn extends React.Component {
    constructor() {
        super()

        this.openModal = this.openModal.bind(this)
    }

    openModal(ev) {
        let modal = $("#add_duration")
        modal.find("h5").text(this.props.taskName)
        modal.find("h6").text(this.props.taskId)
        modal.modal('open')
    }

    render() {
        return (
            <button className="waves-effect waves-light btn grey darken-4" onClick={() => this.openModal()}>
                <i className="material-icons left">timer</i>
                    Duration 
            </button>
        )
    }
}

class AddDurationModal extends React.Component {
    constructor() {
        super()

        this.saveDuration = this.saveDuration.bind(this)
        this.state = {
            isLoading: false
        }
    }
    saveDuration(ev) {
        this.setState({
            isLoading: true
        })

        let taskIdentity = $($("h6.hide")[0]).html()
        let hours = $("#hours").val()
        let token = getToken()
        let modal = $("#add_duration")
        const dispatch = this.props.dispatch

        fetch(`/rooms/update_task/${taskIdentity}/duration/`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${token}`
            },
            body: JSON.stringify({
                duration: parseInt(hours),
            })
        }).then((response) => {
            return response.status
        }).then((status) => {
            dispatch(fetchTasks())
            this.setState({
                isLoading: false
            })
            modal.modal('close');
        }).catch((err) => {
            console.log(err)
        }).catch((err) => {
            console.log(err)
        })
    }
    render() {
        var isLoading = this.state.isLoading
        return (
            <div id="add_duration" className="modal">
                <h6 className="hide"></h6>
                <div className="modal-content">
                    <h5 id="task_name"></h5>
                    <p> Duration in hours </p>
                    <p className="range-field">
                        <input type="range" id="hours" min="0" max="500"/>
                    </p>
                </div>
                <div className="modal-footer">
                    { isLoading ? (
                        <PreLoader /> 
                    ) : ( 
                        <ModalFooterButtons onUpdateTask={ this.saveDuration }/>
                    )}
                </div>
            </div>
        )
    }
}

class ModalFooterButtons extends React.Component {
    constructor() {
        super()
    }

    render() {
        return (
            <div>
                <button className="btn-flat waves-green waves-effect waves-green" onClick={ this.props.onUpdateTask }>
                    <i className="material-icons left">save</i>
                    save
                </button>
                <button className="btn-flat waves-green waves-effect waves-green modal-action modal-close">
                    <i className="material-icons left">cancel</i>
                    cancel
                </button>
            </div>
        )
    }
}

export default NewTasks
