import React from 'react'

import Message from './message'

class MessageList extends React.Component {
    constructor() {
        super();
    }

    render() {
        
        var messageNodes = this.props.messages.map((message) => {
            return (
                <div className="card-panel" key={message.id}>
                    <Message message={ message } />
                </div>
            )
        })

        return (
            <div>
                { messageNodes }
            </div>
        )
    }
}

export default MessageList
