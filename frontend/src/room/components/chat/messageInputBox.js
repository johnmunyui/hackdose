import React from 'react'
import {
    connect,
    sendMessage
} from '../../actions/index'

import {
    getToken,
    getRoom
} from '../../api/urlSplit'


class MessageInputBox extends React.Component {
    constructor() {
        super();

        this.getMessage = this.getMessage.bind(this)
    }

    getMessage(ev) {
        // check if key pressed is enter
        if (ev.which == 13 || ev.keyCode == 13) {
            var message = ev.target.value
            ev.target.value = ""
            this.props.dispatch(sendMessage(message))
            Materialize.toast('Message sent!', 3000, 'rounded')
        }
    }
    componentDidMount() {
        let room = getRoom()
        let token = getToken()
        this.props.dispatch(connect(room, token))
    }
    render() {
        return (
            <div id="message-box">
                    <div className="row">
                        <div className="input-field col m12" id="message-input">
                            <i className="material-icons prefix">mode_edit</i>
                            <textarea id="msg" className="materialize-textarea" onKeyDown={ this.getMessage } placeholder="message"></textarea> 
                        </div>
                    </div>
                </div>
        )
    }
}

export default MessageInputBox
