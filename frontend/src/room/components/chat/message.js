import React from 'react'
import TimeAgo from 'timeago-react'


class Message extends React.Component {
    constructor() {
        super()
    }

    render() {
        const {
            message
        } = this.props
        return (
            <div className="row">
                <div className="col s2">
                    <span className="new badge" data-badge-caption={ message.sender } > </span>
                </div>
                <div className="col s8">
                    <span className="blue-text text-darken-2">
                        { message.message }
                    </span>
                </div>
                <div className="col s2">
                    <TimeAgo datetime={ message.timestamp } />
                </div>
            </div>
        )
    }
}

export default Message
