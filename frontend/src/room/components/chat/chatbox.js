import React from 'react'

import MessageInputBox from './messageInputBox'
import MessageList from './messageList'
import {
    fetchTodaysMessages
} from '../../actions'

import {
    getRoom
} from '../../api/urlSplit'


class ChatBox extends React.Component {
    constructor() {
        super();
    }
    componentDidMount() {
        const { dispatch } = this.props 
        var room = getRoom()

        dispatch(fetchTodaysMessages())
    }
    render() {
        const style = {
            "display": "none"
        }

        const {
            dispatch
        } = this.props
        return (
            <main id="chat-main" style={style}>
                    <div className="row">
                        <div className="col s12 m12 l10">
                            <MessageInputBox dispatch={ dispatch } />
                            <div id="spacer">
                            </div>
                            <MessageList messages={ this.props.messages } /> 
                        </div>
                    </div> 
                </main>
        )
    }
}

export default ChatBox
