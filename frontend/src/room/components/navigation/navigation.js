import React from 'react'
import {
    renderUI
} from '../../api/uiDisplay'

import OnlineUsers from './onlineUsers'

class Navigation extends React.Component {
    constructor() {
        super();
        this.handlePageDisplay = this.handlePageDisplay.bind(this);
    }

    handlePageDisplay(ev) {
        ev.preventDefault()
        renderUI(ev.target.id)
    }
    render() { 
        return (
            <header> 
                <div className="container">
                    <a href="#" data-activates="nav-mobile" className="button-collapse top-nav full hide-on-large-only">
                        <i className="material-icons"> menu </i>
                    </a>
                </div>
                <ul id="nav-mobile" className="side-nav fixed">
                    <li className="logo">
                        <a id="logo-container" href="/" className="brand-logo">
                            <img src="/static/img/icons/ms-icon-70x70.png" id="front-page-logo"/> 
                        </a>
                    </li>
                    <li className="bold">
                        <a href="#" id="chat" className="waves-effect" onClick={this.handlePageDisplay}>
                            Chat
                        </a>
                    </li>
                    <li className="bold">
                        <a href="#" id="task" className="waves-effect" onClick={this.handlePageDisplay}>
                            Tasks
                        </a>
                    </li> 
                    <li>
                        <div className="card-panel">
                            <span className="black-text"> { this.props.room } </span>
                        </div>
                    </li>
                    <li>
                    </li>
                    <OnlineUsers users={ this.props.users } /> 
                </ul> 
            </header>
        )
    }
}

module.exports = Navigation
