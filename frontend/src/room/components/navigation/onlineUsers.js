import React from 'react'


class OnlineUsers extends React.Component {
    constructor() {
        super();
    }

    render() {
        var onlineUsers = this.props.users.map((user) => {
            return (
                <div className="chip" key={user}>
                    <i className="face-icon material-icons">face</i>
                    { user }
                </div>
            )
        })

        return (
            <li>
                { onlineUsers }
            </li>
        )
    }
}

export default OnlineUsers
