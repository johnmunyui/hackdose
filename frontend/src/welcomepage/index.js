import {
    render
} from 'react-dom'
import React from 'react'

import WelcomePage from './components/welcome.js'

render( < WelcomePage / > , document.getElementById('welcome'))
