import React from 'react'

class WelcomePage extends React.Component {
    constructor() {
        super();

        this.state = {
            rooms: null,
            token: null,
        }

        this.loadRoomsFromTheServer = this.loadRoomsFromTheServer.bind(this)
    }
    loadRoomsFromTheServer(token) {
        var state = this
        $.ajax({
            url: "/rooms/",
            method: "GET",
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', `Token ${token}`)
            },
            success: function(response) {
                var data = JSON.parse(response)
                state.setState({
                    rooms: data
                })
            }
        })
    }
    componentWillMount() {
        // grab the hidden p tag with the token

        var tokenTag = $("#token")
            // get the token
        var tokenString = tokenTag.text().trim()
        this.setState({
            token: tokenString
        })

        this.loadRoomsFromTheServer(tokenString)
    }
    render() {
        var rooms = this.state.rooms
        if (rooms == null || rooms.length == 0) {
            return (
                <NoRoomFound token={ this.state.token }/>
            )
        } else {
            return (
                <RoomsFound rooms={ this.state.rooms } token={ this.state.token } />
            )
        }
    }
}


class NoRoomFound extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <div className = "row">
           		 <div className = "col s12 m6 offset-m3" >
            			<div className = "card" >
            				<div className = "card-content black-text" >
						        <span className = "card-title" > No room found </span> 
						        <p> Unfortunately, we did not find any room that you are a member of.You can create a new room and  members using the link below. </p> 
					        </div> 
		    			<div className = "card-action" >
						    <a href = {`/rooms/create_room/${this.props.token}`} className = "black-text" > create a room </a> 
					    </div> 
				    </div> 
			    </div> 
		    </div>
        )
    }
}

class RoomsFound extends React.Component {
    constructor() {
        super();
    }
    render() {
        var token = this.props.token
        var groupNodes = this.props.rooms.map(function(room){
            var roomUrl = room.name.replace(/ /g, '+').toLowerCase()
            return (
                <div className="col s12 m4" key={room.id} style={{marginTop: "10px"}}>
                    <a className="waves-effect waves-light btn grey darken-4" href={`/rooms/hackon/${roomUrl}/${token}/`}>
                        <i className="material-icons left">people</i>
                        { room.name }
                    </a>
                </div>
            )
        })
        return (
            <div className="row" style={{marginTop: "10px"}}>
                <div className="col s12 m12">
                    <div className="card-panel grey darken-4">
                        <span className="white-text"> Welcome, Click any of the group(s) below to enter </span>
                    </div> 
                </div>
                { groupNodes }
            </div>
        )
    }
}
export default WelcomePage
