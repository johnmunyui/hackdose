module.exports = {
    collectRoomInformation: function() {
        var roomInfo = {}
        var basicInfo = $("#basic")

        roomInfo['name'] = basicInfo.find("input")[0].value.trim()
        roomInfo['description'] = basicInfo.find("textarea")[0].value.trim()

        roomInfo['tasks'] = []
        var taskInfo = $("#tasks")
        var tasks = taskInfo.find("a")
        for (var task of tasks) {
            roomInfo['tasks'].push($(task).children().remove().end().text().trim())
        }

        roomInfo['hackers'] = []
        var hackersInfo = $("#hackers")
        var hackers = hackersInfo.find("a")
        for (var hacker of hackers) {
            roomInfo['hackers'].push($(hacker).children().remove().end().text().trim())
        }

        var errors = validate_data(roomInfo)

        if ($.isEmptyObject(errors)) {
            return roomInfo
        } else {
            return errors
        }
    }
}

function validate_data(roomInfo) {
    var errors = {}
    if (!roomInfo['name']) {
        errors['basic'] = "please enter the name of the room"
    }

    if (roomInfo['tasks'].length == 0) {
        errors['tasks'] = "please enter at least one task"
    }

    if (roomInfo['hackers'].length == 0) {
        errors['hackers'] = "please enter at least one programer"
    }

    return errors
}
