import React from 'react'
require('materialize-css/dist/js/materialize.js');
require('materialize-css/js/init.js');

class Hackers extends React.Component {
    constructor() {
        super();

        this.state = {
            hackers: []
        }

        this.addHacker = this.addHacker.bind(this)
        this.deleteHacker = this.deleteHacker.bind(this)
    }
    addHacker(hacker) {
        var hackers = this.state.hackers

        hackers.push(hacker)

        this.setState({
            'hackers': hackers
        })
    }
    deleteHacker(hacker) {
        var hackers = this.state.hackers
        var idx = hackers.indexOf(hacker)
        delete hackers[idx]

        this.setState({
            'hackers': hackers
        })
    }
    render() {
        return (
            <div className="row" id="hackers">
                <AddHacker onAddHacker={ this.addHacker } token={ this.props.token }/>
		        <HackersList data={this.state.hackers} onDeleteHacker={ this.deleteHacker }/>
		    </div>
        );
    }
}

class HackersList extends React.Component {
    constructor() {
        super();

        this.removeHacker = this.removeHacker.bind(this)
    }
    removeHacker(hacker) {
        this.props.onDeleteHacker(hacker)
    }
    render() {
        var removeHacker = this.removeHacker
        var hackerNodes = this.props.data.map(function(hacker, index) {
            return (
                <div key={ index }>
			        <Hacker hacker={ hacker } onRemoveHacker = { removeHacker }/>
		        </div>
            );
        })

        return (
            <div className="collection">
				{ hackerNodes }
			</div>
        );
    }
}

class Hacker extends React.Component {
    constructor() {
        super();

        this.removeHacker = this.removeHacker.bind(this)
    }
    removeHacker(ev) {
        var parent = $(ev.target).parent()[0]
        var hacker = $(parent).clone().children().remove().end().text()
        this.props.onRemoveHacker(hacker)
    }
    render() {
        return (
            <a className="collection-item" href="javascript:void(0)"> 
				{ this.props.hacker }
				<i className="material-icons prefix right" onClick={ this.removeHacker }>delete</i>
			</a>
        );
    }
}

class AddHacker extends React.Component {
    constructor() {
        super();

        this.state = {
            'suggested_users': {}
        }

        this.handleReturnPressed = this.handleReturnPressed.bind(this)
        this.loadUsernamesFromServer = this.loadUsernamesFromServer.bind(this)
        this.displaySuggestedUsernames = this.displaySuggestedUsernames.bind(this)
    }
    displaySuggestedUsernames() {
        console.log("wow")
        var state = this.state
        console.log(state)
        $('input.autocomplete').autocomplete({
            data: state.suggest_users,
            limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
            minLength: 1,
        });
    }
    loadUsernamesFromServer(username) {
        var token = this.props.token
        if ((username.length + 1) % 3 != 0)
            return

        $.ajax({
            url: `/user/suggest_users/${username}/`,
            type: "GET",
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', `Token ${token}`)
            },
            success: function(response) {
                console.log(response)
                var autocomplete = {}
                for (var user of response) {
                    autocomplete[user.username] = null
                }
                this.setState({
                    'suggest_users': autocomplete
                })
                this.displaySuggestedUsernames()
            }.bind(this),
            error: function(xhr, status, error) {
                console.error('', status, error.toString());
            }
        })
    }
    handleReturnPressed(ev) {
        var event = ev || window.event;
        var charCode = event.which || event.keyCode;

        var newHacker = event.target.value

        if (!newHacker) {
            return
        }

        this.loadUsernamesFromServer(newHacker)
        if (charCode == '13') {
            this.props.onAddHacker(newHacker)
            ev.target.value = ""
        }
    }
    render() {
        return (
            <div className="row" id="hackers">
			    <div className="input-field col m12">
				    <input placeholder="search by email" id="hacker" type="text" onKeyPress={ this.handleReturnPressed } className="autocomplete"></input>
				    <label htmlFor="hacker"> hackers </label>
			    </div>	
			</div>
        );
    }
}

export default Hackers
