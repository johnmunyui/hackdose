import React from 'react'
import Tasks from './tasks'
import Hackers from './hackers'
import Basic from './basic'
import {
    collectRoomInformation
} from '../utils/utilities'

class CreateRoom extends React.Component {
    constructor() {
        super();

        this.state = {
            roomInformation: null,
            token: null,
            errors: null
        }

        this.saveRoom = this.saveRoom.bind(this)
        this.postRoomToServer = this.postRoomToServer.bind(this)
    }

    componentDidMount() {
        var url = window.location.href
        var token = url.split("/")[5]
        this.setState({
            'token': token
        })
    }
    postRoomToServer(data) {
        var token = this.state.token
        $.ajax({
            url: "/rooms/create_room/",
            type: "POST",
            data: JSON.stringify(data),
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', `Token ${token}`)
            },
            success: function(response) {
                var url = `/welcome/${token}/`
                window.location.href = url
            }
        })
    }
    saveRoom(ev) {
        var feedBack = collectRoomInformation()
        this.postRoomToServer(feedBack)
    }
    render() {
        return (
            <div className="col s12 m10">
		        <div className="card">
		        	<div className="card-content">
				        <span className="card-title"> Create Room </span>
				        <button className="btn waves-effect waves-light grey darken-4 right" type="submit" onClick={ this.saveRoom }>Create
					        <i className="material-icons right">send</i>
			    	    </button>
			        </div>
			        <div className="card-content">
				        <div className="card-tabs">
					        <ul className="tabs tabs-fixed-width">
						        <li className="tab"> <a href="#basic" className="active black-text"> Basic </a> </li>
						        <li className="tab"> <a href="#tasks" className="black-text"> Tasks </a> </li>
						        <li className="tab"> <a href="#hackers" className="black-text"> Hackers </a></li>
					        </ul>
				        </div>
			        </div>
			        <div className="card-content">
				        <Basic />
				        <Tasks />
				        <Hackers token={ this.state.token }/>
			        </div>
		        </div>
	        </div>
        );
    }
}

export default CreateRoom
