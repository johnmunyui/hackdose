import React from 'react'

class Tasks extends React.Component {
	constructor() {
		super();

		this.state = {
			tasks: [],
		}

		this.addTask = this.addTask.bind(this)
		this.deleteTask = this.deleteTask.bind(this)
	}

	addTask(task) {
		var tasks = this.state.tasks
		tasks.push(task)

		this.setState({'tasks': tasks})
	}

	deleteTask(task) {
		var tasks = this.state.tasks
		var idx = tasks.indexOf(task)
		
		delete tasks[idx]
		this.setState({'tasks': tasks})
	}

	render() {
		return (
            <div className="row" id="tasks">
			    <AddTask onAddTask={this.addTask}  />
			    <TaskList data={this.state.tasks} onDeleteTask={ this.deleteTask } />
		    </div>
		);
	}
}

class AddTask extends React.Component {
	constructor(props) {
		super(props);

		this.handleReturnPressed = this.handleReturnPressed.bind(this);
	}
	handleReturnPressed(ev) {
		var event = ev || window.event;
		var charCode = event.which || event.keyCode;

		var newTask = event.target.value

		if (!newTask) {
			return
		}

		if (charCode == '13') {

			this.props.onAddTask(newTask)
			ev.target.value = ""
    	}

	}
	render() {
		return (
            <div className="input-field col m12">
			    <input placeholder="eg ui for books" id="task" type="text" className="validate" onKeyPress={ this.handleReturnPressed }></input>
			    <label htmlFor="task"> task </label>
		    </div>
		);
	}

}

class TaskList extends React.Component {
	constructor() {
		super();

		this.removeTask = this.removeTask.bind(this)
	}

	removeTask (task) {
		this.props.onDeleteTask(task)
	} 
	render() {
		var  removeTask = this.removeTask
		var taskNodes = this.props.data.map(function(task, index) {
			return (
			    <div key={index}>
				    <Task task={task} onRemoveTask={ removeTask }/>
			    </div>
			);
		})
        
        if (this.props.data.length == 0){
            return (
                <h6> No tasks added </h6>
            )
        }
		return (
            <div className="collection">
			    { taskNodes }
		    </div>
		)
	}
}

class Task extends React.Component {
	constructor() {
		super();

		this.removeTask = this.removeTask.bind(this)
	}
	removeTask(ev) {
		var parent = $(ev.target).parent()[0]
		var task = $(parent).clone().children().remove().end().text()
		this.props.onRemoveTask(task)
	}
	render() {
		return (
            <a href="javascript:void(0)"className="collection-item">
                { this.props.task }
				<i className="material-icons prefix right" onClick={ this.removeTask }>delete</i>
			</a>
		)
	}
}

export default Tasks
