import React from 'react'

class Basic extends React.Component {
    constructor() {
        super();

        this.state = {
            name: null
        }
    }

    render() {
        return (
            <div className = "row" id = "basic" >
                <div className = "input-field col m12" >
            	    <input placeholder = "eg transformers" id = "name" type = "text"className = "validate" ></input> 
				    <label htmlFor = "name" > name </label>
			    </div> 
			    <div className = "input-field col m12" >
           		    <textarea placeholder = "small description" id = "description" type = "text" className = "materialize-textarea" ></textarea> 
			 	    <label htmlFor = "description" > description </label> 
			    </div>	 
            </div>
        );
    }
}

export default Basic
