import React from 'react'

class LogInForm extends React.Component {
    constructor() {
        super();
        this.state = {
            username: null,
            password: null
        }
    }
    login(username, password) {
        var alertbox = this.refs.alertbox

        var data = {
            "username": username,
            "password": password
        }

        var alertbox = this.refs.alertbox
        var errormessage = this.refs.errormessage



        $.ajax({
            url: '/user/login',
            type: 'POST',
            data: JSON.stringify(data),
            success: function(result) {
                console.log(result)
                window.location.href = "/welcome/" + result.token;
            }.bind(this),
            error: function(xhr, status, error) {
                alertbox.style.display = "block"
                errormessage.innerHTML = "can not log in using the provided credentials"
                console.log(status, error)
            }.bind(this)
        })
    }
    handleClick() {
        var alertbox = this.refs.alertbox
        alertbox.style.display = "none"
        var username = this.refs.username.value.trim()
        var password = this.refs.password.value.trim()

        if (!username || !password) {
            alertbox.style.display = "block"
            var errormessage = this.refs.errormessage
            errormessage.innerHTML = "please enter username and password"
        }

        this.login(username, password)
    }
    render() {
        return (
            <div className = "col m6 offset-m2" >
                <div className = "card" >
                    <div className = "card-content" >
                        <span className = "card-title" > Log in </span> 
                            <div className = "row" >
                                <div className = "input-field" >
                                    <i className = "material-icons prefix" > account_circle </i> 
                                    <input placeholder = "optimous" id = "username" type = "text" className = "validate"ref = "username" ></input>
                                    <label htmlFor = "username" > username </label> 
                                </div>
                                <div className = "input-field" >
                                    <i className = "material-icons prefix" > lock_circle </i> 
                                    <input placeholder = "******" id = "password" type = "password" className = "validate" ref = "password" ></input>
                                    <label htmlFor = "password" > password </label> 
                                </div> 
                                <div className = "input-field" >
                                    <div className = "error-alert" style = {{display: 'none'}} ref = "alertbox" >
                                        <p ref = "errormessage" > Dude </p> </div> 
                                    </div> 
                                <div className = "input-field" >
                                    <button className = "waves-effect waves-light btn grey darken-4" id = "loginbtn" onClick = {() => this.handleClick()} >
                                        <i className = "material-icons left" > done_all </i>
                                        log in
                                    </button> 
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                </div>
        );
    }
}

export default LogInForm;
