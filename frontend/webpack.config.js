var webpack = require("webpack");
var path = require("path");
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var BUILD_DIR = path.resolve(__dirname, "../app/public/js/");
var APP_DIR = path.resolve(__dirname, 'src');

var config = {
    entry: {
        login: APP_DIR + "/authentication/index.js",
        welcome: APP_DIR + "/welcomepage/index.js",
        roomCreate: APP_DIR + "/roomcreate/index.js",
        room: APP_DIR + "/room/index.js",
    },
    output: {
        path: BUILD_DIR,
        filename: "[name].bundle.js",
        chunkFilename: "[id].chunk.js"
    },
    module: {
        loaders: [{
            test: /\.jsx?/,
            include: APP_DIR,
            loader: 'babel'
        }, {
            test: /\.css$/,
            loader: "style-loader!css-loader"
        }, {
            test: /\.(jpg|png|gif)$/,
            loader: "file-loader?name=images/[hash].[ext]"
        }, {
            test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            loader: "url-loader?limit=80000&minetype=application/font-woff"
        }, {
            test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            loader: "url-loader?limit=80000"
        }]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            'window.jQuery': "jquery",
            'window.$': "jquery",
            Hammer: "hammerjs/hammer"
        })
    ],
    resolve: {
        extentions: ['', '.js', '.jsx', '.css'],
        alias: {
            jquery: path.join(__dirname, 'node_modules/jquery/dist/jquery'),
        },
        modulesDirectories: [
            'node_modules'
        ]
    },
};

module.exports = config;
