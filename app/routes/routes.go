package routes

import (
	"net/http"
	"os"

	"bitbucket.org/johnmunyui/hackdose/app/chat"
	"bitbucket.org/johnmunyui/hackdose/app/config"
	"bitbucket.org/johnmunyui/hackdose/app/controllers"
	"bitbucket.org/johnmunyui/hackdose/app/middleware"
	"github.com/julienschmidt/httprouter"
)

func Routes() (router *httprouter.Router) {
	router = httprouter.New()

	goPath := os.Getenv("GOPATH")
	// static file path
	staticFilePath := goPath + "/src/bitbucket.org/johnmunyui/hackdose/app/public/"
	// serve static files
	router.ServeFiles("/static/*filepath", http.Dir(staticFilePath))

	// index page router
	router.GET("/", controllers.IndexPage)

	// authentication endpoints
	uc := controllers.NewUserController(config.GetSession())
	// Check if username exists
	router.GET("/user/check_username/:user_name/", uc.CheckIfUserNameExists)

	// create account
	router.POST("/user/create_account", uc.RegisterUser)

	// suggest users. Used when adding users to a room
	router.GET(middleware.JWTCheker("/user/suggest_users/:search_term/", uc.SuggestUsers))
	// log in
	router.GET("/user/login", uc.LogIn)
	router.POST("/user/login", uc.LogIn)
	// user profile
	router.GET(middleware.JWTCheker("/user/profile/:token/", uc.Profile))

	//welcome page
	router.GET(middleware.JWTCheker("/welcome/:token/", controllers.Welcome))

	//return all user rooms
	rc := controllers.NewRoomController(config.GetSession())
	router.GET(middleware.JWTCheker("/rooms/", rc.GetUserRooms))
	router.GET(middleware.JWTCheker("/rooms/create_room/:token/", rc.CreateRoom))
	router.POST(middleware.JWTCheker("/rooms/create_room/", rc.CreateRoom))
	router.GET(middleware.JWTCheker("/rooms/hackon/:room/:token/", rc.Room))
	router.GET(middleware.JWTCheker("/rooms/task_list/:room/", rc.TaskList))

	// update parameter represents what you want to update
	// either a task has been taken, completed, etc
	router.PATCH(middleware.JWTCheker("/rooms/update_task/:task_id/:update/", rc.UpdateTask))
	router.PUT(middleware.JWTCheker("/rooms/add_more_tasks/:room_id/", rc.AddMoreTasks))

	// Chat
	hub := chat.NewHub()
	go hub.Run()

	router.GET("/chat/:room/:token/", func(res http.ResponseWriter, req *http.Request, ps httprouter.Params) {
		chat.ChatHandler(hub, res, req, ps)
	})

	// retrieve today's messages
	router.GET(middleware.JWTCheker("/chats/retrieve_todays_messages/:room/", chat.RetrieveTodaysMessages))
	return
}
