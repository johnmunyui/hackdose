package chat

import "encoding/json"

type message struct {
	data []byte
	room string
}

type subscription struct {
	client *Client
	room   string
}

// hub maintains the set of active clients and broadcasts messages
// to the clients
type Hub struct {
	// Registered clients. With thier rooms
	rooms map[string]map[*Client]bool

	// Inbound messages from clients
	broadcast chan message

	// register requests from the clients
	register chan *subscription

	// unregister requests from the clients
	unregister chan *subscription
}

func (hub *Hub) onlineClients(room string) {
	// Returns the online members of a particular group/room
	connection := hub.rooms[room]

	var users []string
	for client := range connection {
		users = append(users, client.user)
	}

	roomOnlineClients := OnlineClients{
		Type:    "ROOM_STATUS",
		Room:    room,
		Members: users,
	}

	sendData, err := json.Marshal(roomOnlineClients)

	if err != nil {
		panic(err)
	}

	for client := range connection {
		client.send <- sendData
	}

}

// initialize the hub struct and return a pointer
func NewHub() *Hub {
	return &Hub{
		broadcast:  make(chan message),
		register:   make(chan *subscription),
		unregister: make(chan *subscription),
		rooms:      make(map[string]map[*Client]bool),
	}
}

// Hub type method
func (hub *Hub) Run() {
	for {
		select {
		// Register a client. This is if the register channell
		// has any data.
		case sub := <-hub.register:
			connection := hub.rooms[sub.room]
			if connection == nil {
				connection = make(map[*Client]bool)
				hub.rooms[sub.room] = connection
			}
			hub.rooms[sub.room][sub.client] = true
			// get the online clients and send them to
			// all other clients
			hub.onlineClients(sub.room)

		// Unregister a client. This is if the unregister
		// client has any data
		// Delete them from the clients map
		case sub := <-hub.unregister:
			connection := hub.rooms[sub.room]
			if connection != nil {
				if _, ok := connection[sub.client]; ok {
					delete(connection, sub.client)
					close(sub.client.send)
					if len(connection) == 0 {
						delete(hub.rooms, sub.room)
					}
				}
			}
			hub.onlineClients(sub.room)

			// Broadcast ie send message to all.
			// This is if the broadcast channel has any
			// data
			// all connected clients
		case message := <-hub.broadcast:
			connection := hub.rooms[message.room]
			for client := range connection {
				select {
				case client.send <- message.data:
				default:
					close(client.send)
					delete(connection, client)
					if len(connection) == 0 {
						delete(hub.rooms, message.room)
					}
				}
			}
		}
	}
}
