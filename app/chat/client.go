package chat

import (
	"bytes"
	"encoding/json"
	"log"
	"time"

	"bitbucket.org/johnmunyui/hackdose/app/config"
	"github.com/gorilla/websocket"
	"gopkg.in/mgo.v2/bson"
)

const (
	// Time allowed to write a message to the peer
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait
	pingPeriod = (pongWait * 9) / 10

	// Maximum message Size allowed from peer
	maxMessageSize = 1024
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// Client is a middleman between a websocket connection and the hub
type Client struct {
	hub *Hub
	// The websocket connection
	conn *websocket.Conn

	// buffered channel for outbound messages
	send chan []byte

	// unique name of the client
	user string

	//room id
	roomId bson.ObjectId
}

// readPump pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.

func (sub *subscription) readPump() {
	client := sub.client
	defer func() {
		client.hub.unregister <- sub
		client.conn.Close()
	}()

	client.conn.SetReadLimit(maxMessageSize)
	client.conn.SetReadDeadline(time.Now().Add(pongWait))
	client.conn.SetPongHandler(func(string) error { client.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })

	for {

		// try reading a message from the websocket connection
		_, clientMessage, err := client.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway) {
				log.Printf("error: %v ", err)
			}
			break
		}

		clientMessage = bytes.TrimSpace(bytes.Replace(clientMessage, newline, space, -1))
		messageToSave := Message{
			ID:          bson.NewObjectId(),
			Sender:      client.user,
			Type:        "CHAT_MESSAGE",
			TextMessage: string(clientMessage),
			Timestamp:   time.Now(),
			RoomId:      client.roomId,
		}
		go func() {
			session := config.GetSession()
			err := session.DB("hackdose").C("messages").Insert(messageToSave)

			if err != nil {
				panic(err)
			}

		}()

		// if reading the message was successfull, pass the read message to
		// the hub's broadcast channel. It will broadcast the message to all
		// connected clients
		sendData, _ := json.Marshal(messageToSave)
		mess := message{sendData, sub.room}
		client.hub.broadcast <- mess
	}
}

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.

func (sub *subscription) writePump() {
	client := sub.client
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		client.conn.Close()
	}()

	for {
		select {
		case message, ok := <-client.send:
			client.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// Hub closed the channel
				client.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			writer, err := client.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}

			writer.Write(message)
			// Add queued chat messages to the current websocket message.
			messages := len(client.send)

			for i := 0; i < messages; i++ {
				writer.Write(newline)
				writer.Write(<-client.send)
			}

			if err := writer.Close(); err != nil {
				return
			}
		case <-ticker.C:
			client.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := client.conn.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
				return
			}
		}
	}
}
