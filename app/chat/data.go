package chat

import (
	"fmt"
	"time"

	"bitbucket.org/johnmunyui/hackdose/app/config"
	"gopkg.in/mgo.v2/bson"
)

type OnlineClients struct {
	Type    string   `json:"type"`
	Room    string   `json:"room"`
	Members []string `json:"members"`
}

type Message struct {
	ID          bson.ObjectId `json:"id" bson:"_id"`
	Type        string        `json:"type" bson:"type"`
	Sender      string        `json:"sender" bson:"sender"`
	TextMessage string        `json:"message" bson:message`
	Timestamp   time.Time     `json:"timestamp" bson:"timestamp"`
	RoomId      bson.ObjectId `json:"room_id" bson:"room_id"`
}

func retrieveTodaysMessages(roomId bson.ObjectId) []Message {
	now := time.Now()
	today := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())

	messages := []Message{}

	fmt.Println(today)

	query := bson.M{
		"room_id": roomId,
		"timestamp": bson.M{
			"$gte": today,
		},
	}

	dbConnection := config.GetSession().DB("hackdose").C("messages")

	err := dbConnection.Find(query).All(&messages)

	if err != nil {
		panic(err)
	}

	return messages
}
