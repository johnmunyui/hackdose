package chat

import (
	"encoding/json"
	"net/http"
	"strings"

	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/johnmunyui/hackdose/app/config"
	"bitbucket.org/johnmunyui/hackdose/app/models"
	"bitbucket.org/johnmunyui/hackdose/app/utilities"
	"github.com/julienschmidt/httprouter"
)

func ChatHandler(hub *Hub, res http.ResponseWriter, req *http.Request, ps httprouter.Params) {
	// upgrade the http connection to a web socket connection
	conn, err := upgrader.Upgrade(res, req, nil)

	if err != nil {
		panic(err)
	}

	token := ps.ByName("token")

	room := ps.ByName("room")

	if strings.Contains(room, "%20") {
		room = strings.Replace(room, "%20", " ", -1)
	} else {
		room = strings.Replace(room, "+", " ", -1)
	}

	valid, user := config.ValidateToken(token)

	if !valid {
		panic("invalid token")
	}

	userRoom := models.Room{}
	session := config.GetSession()
	dbConnection := session.DB("hackdose").C("rooms")
	err = dbConnection.Find(bson.M{"name": bson.RegEx{room, "i"}}).One(&userRoom)

	if err != nil {
		panic(err)
	}

	// Create the client
	client := &Client{
		hub:    hub,
		send:   make(chan []byte, 256),
		conn:   conn,
		user:   user,
		roomId: userRoom.ID,
	}

	// add the client to the correct subscription
	// based on the group
	sub := &subscription{
		client,
		room,
	}

	// Add client data to the register channel.
	// Hub will add the client.
	client.hub.register <- sub

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go sub.writePump()
	go sub.readPump()

}

func RetrieveTodaysMessages(res http.ResponseWriter, req *http.Request) {
	params := utilities.FetchParams(req)
	roomName := params.ByName("room")

	if strings.Contains(roomName, "%20") {
		roomName = strings.Replace(roomName, "%20", " ", -1)
	} else {
		roomName = strings.Replace(roomName, "+", " ", -1)
	}

	room := models.Room{}
	dbConnection := config.GetSession().DB("hackdose").C("rooms")
	err := dbConnection.Find(bson.M{"name": bson.RegEx{roomName, "i"}}).One(&room)

	if err != nil {
		panic(err)
	}

	todaysMessages := retrieveTodaysMessages(room.ID)

	sendData, _ := json.Marshal(todaysMessages)

	res.Header().Set("Content-Type", "application/json")
	res.Write(sendData)
}
