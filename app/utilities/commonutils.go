package utilities

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
)

func FetchParams(req *http.Request) httprouter.Params {
	ctx := req.Context()
	return ctx.Value("params").(httprouter.Params)
}
