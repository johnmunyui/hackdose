package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"bitbucket.org/johnmunyui/hackdose/app/config"
	"bitbucket.org/johnmunyui/hackdose/app/models"
	"bitbucket.org/johnmunyui/hackdose/app/templates"
	"bitbucket.org/johnmunyui/hackdose/app/utilities"
	"github.com/julienschmidt/httprouter"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type userController struct {
	session *mgo.Session
}

type loginPayload struct {
	Username string `json: username`
	Password string `json: password`
}

type response map[string]string

type suggestUsers struct {
	Username string `json:"username"`
	Email    string `json:"email"`
}

func NewUserController(session *mgo.Session) *userController {
	return &userController{session}
}

func (uc *userController) CheckIfUserNameExists(res http.ResponseWriter, req *http.Request, params httprouter.Params) {
	userName := params.ByName("user_name")
	dbConnection := uc.session.DB("hackdose").C("users")

	count, err := dbConnection.Find(bson.M{"username": bson.RegEx{userName, "i"}}).Count()

	if err != nil {
		panic(err)
	}

	userExists := make(map[string]bool)
	if count == 0 {
		userExists["exists"] = false
	} else {
		userExists["exists"] = true
	}

	responseData, _ := json.Marshal(userExists)

	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(http.StatusOK)

	res.Write(responseData)

}
func (uc *userController) RegisterUser(res http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	password, err := bcrypt.GenerateFromPassword([]byte(req.PostFormValue("password")), bcrypt.DefaultCost)

	if err != nil {
		panic(err)
	}

	user := models.User{}
	user.UserName = req.PostFormValue("username")
	user.PasswordHash = string(password)
	user.Email = req.PostFormValue("email")
	user.DateCreated = time.Now()
	user.IsActive = true

	err = uc.session.DB("hackdose").C("users").Insert(user)

	token := config.GetToken(user.UserName)
	profileUrl := fmt.Sprintf("/user/profile/%s/", token)
	if err != nil {
		panic(err)
	}

	http.Redirect(res, req, profileUrl, http.StatusSeeOther)
}

func (uc *userController) LogIn(res http.ResponseWriter, req *http.Request, _ httprouter.Params) {

	if req.Method == "GET" {
		template := templates.New(res)

		template.LoginTpl()
	} else if req.Method == "POST" {
		// holds a connection to the database.
		dbConnection := uc.session.DB("hackdose").C("users")

		userData := loginPayload{}

		jsn, err := ioutil.ReadAll(req.Body)

		if err != nil {
			panic(err)
		}

		err = json.Unmarshal(jsn, &userData)

		if err != nil {
			panic(err)
		}

		user := models.User{}
		err = dbConnection.Find(bson.M{"username": userData.Username}).One(&user)
		if err != nil {
			panic(err)
		}

		err = bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(userData.Password))
		if err != nil {
			res.Header().Set("Content-Type", "application/json")
			res.WriteHeader(403)
			response_data := response{"error": "can not log in with the provided credentiatls"}
			res_data, _ := json.Marshal(response_data)
			res.Write(res_data)
		} else {
			// update user's last login
			userToUpdate := bson.M{"username": userData.Username}
			updateLastLogin := bson.M{"$set": bson.M{"last_login": time.Now()}}

			err := dbConnection.Update(userToUpdate, updateLastLogin)

			if err != nil {
				panic(err)
			}
			// return a successfully logged in response
			res.Header().Set("Content-Type", "application/json")
			res.WriteHeader(200)
			response_data := make(response)
			response_data["success"] = "successfully logged in user"
			response_data["token"] = config.GetToken(user.UserName)
			res_data, _ := json.Marshal(response_data)
			fmt.Println(res_data)
			res.Write(res_data)
		}
	}
}

func (uc *userController) SuggestUsers(res http.ResponseWriter, req *http.Request) {

	params := utilities.FetchParams(req)
	searchKey := params.ByName("search_term")

	searchKey = fmt.Sprintf("^%s", searchKey)

	dbConnection := uc.session.DB("hackdose").C("users")

	users := []suggestUsers{}

	err := dbConnection.Find(bson.M{"email": bson.RegEx{searchKey, "i"}}).All(&users)

	if err != nil {
		panic(err)
	}

	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(200)

	resData, _ := json.Marshal(users)
	res.Write(resData)
}

func (uc *userController) Profile(res http.ResponseWriter, req *http.Request) {

	params := utilities.FetchParams(req)
	userName := params.ByName("user")
	token := params.ByName("token")

	user := models.User{}
	rooms := []models.Room{}

	dbConnection := uc.session.DB("hackdose")
	dbConnection.C("users").Find(bson.M{"username": userName}).One(&user)
	dbConnection.C("rooms").Find(bson.M{"hackers": userName}).All(&rooms)

	fmt.Println(rooms)

	tplData := templates.ProfileData{UserDetails: user, Rooms: rooms, Token: token}

	template := templates.New(res)
	fmt.Println(user)
	template.ProfileTpl(tplData)
}
