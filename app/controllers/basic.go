package controllers

import (
	"net/http"

	"bitbucket.org/johnmunyui/hackdose/app/templates"
	"bitbucket.org/johnmunyui/hackdose/app/utilities"
	"github.com/julienschmidt/httprouter"
)

func IndexPage(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	template := templates.New(w)
	template.IndexTpl()
}

func Welcome(res http.ResponseWriter, req *http.Request) {
	params := utilities.FetchParams(req)
	user := params.ByName("user")
	token := params.ByName("token")

	templates := templates.New(res)
	templates.WelcomeTpl(user, token)

}
