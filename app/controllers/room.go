package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/johnmunyui/hackdose/app/models"
	"bitbucket.org/johnmunyui/hackdose/app/templates"
	"bitbucket.org/johnmunyui/hackdose/app/utilities"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

/*
New room controller. Handles Rooms creation.
*/

type roomController struct {
	session *mgo.Session
}

// Instantiates a new room controller. Passes in a mongo session for database manipulation.
func NewRoomController(session *mgo.Session) (room *roomController) {
	room = &roomController{session}

	return
}

// rc is an acronym for room controller.
func (rc *roomController) GetUserRooms(res http.ResponseWriter, req *http.Request) {
	dbConnection := rc.session

	params := utilities.FetchParams(req)
	userName := params.ByName("user")

	rooms := []models.Room{}
	dbConnection.DB("hackdose").C("rooms").Find(bson.M{"hackers": userName}).All(&rooms)

	responseData, err := json.Marshal(rooms)

	if err != nil {
		panic(err)
	}

	res.WriteHeader(200)
	res.Header().Set("Content-Type", "application/json")
	res.Write(responseData)

}

func (rc *roomController) CreateRoom(res http.ResponseWriter, req *http.Request) {

	if req.Method == "GET" {
		params := utilities.FetchParams(req)
		token := params.ByName("token")
		user := params.ByName("user")
		res.WriteHeader(200)
		templates := templates.New(res)
		templates.CreateRoomTpl(token, user)

	}

	if req.Method == "POST" {
		jsn, err := ioutil.ReadAll(req.Body)

		params := utilities.FetchParams(req)
		userName := params.ByName("user")

		if err != nil {
			panic(err)
		}
		room := models.Room{}

		err = json.Unmarshal(jsn, &room)

		room.ID = bson.NewObjectId()
		room.DateCreated = time.Now()
		room.Creator = userName
		room.Hackers = append(room.Hackers, userName)

		rc.session.DB("hackdose").C("rooms").Insert(room)

		tempTask := models.Task{}

		for _, task := range room.Tasks {
			tempTask.ID = bson.NewObjectId()
			tempTask.RoomId = room.ID
			tempTask.Name = task
			tempTask.Status = "new"
			tempTask.DateAdded = time.Now()
			tempTask.AddedBy = userName
			err := rc.session.DB("hackdose").C("tasks").Insert(tempTask)

			if err != nil {
				panic(err)
			}
		}

		res.WriteHeader(201)
		res.Header().Set("Content-Type", "application/json")
		res.Write([]byte(`{"success": "room created successfully"}`))
	}

}

func (rc *roomController) Room(res http.ResponseWriter, req *http.Request) {
	params := utilities.FetchParams(req)
	user := params.ByName("user")
	token := params.ByName("token")
	// room := params.ByName("room")

	template := templates.New(res)
	template.RoomTpl(user, token)

}

func (rc *roomController) TaskList(res http.ResponseWriter, req *http.Request) {
	params := utilities.FetchParams(req)
	searchKeyParam := params.ByName("room")

	var searchKey string
	if strings.Contains(searchKeyParam, "+") {
		searchKey = strings.Replace(searchKeyParam, "+", " ", -1)
	} else if strings.Contains(searchKeyParam, "%20") {
		searchKey = strings.Replace(searchKeyParam, "%20", " ", -1)
	} else {
		searchKey = searchKeyParam
	}

	room := models.Room{}
	tasks := []models.Task{}

	dbConnection := rc.session.DB("hackdose")
	dbConnection.C("rooms").Find(bson.M{"name": bson.RegEx{searchKey, "i"}}).One(&room)
	err := dbConnection.C("tasks").Find(bson.M{"roomid": room.ID}).All(&tasks)
	if err != nil {
		panic(err)
	}

	sendData, _ := json.Marshal(tasks)

	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(200)

	res.Write(sendData)
}

func (rc *roomController) UpdateTask(res http.ResponseWriter, req *http.Request) {
	params := utilities.FetchParams(req)
	taskId := params.ByName("task_id")
	whatToUpdate := params.ByName("update")

	jsn, err := ioutil.ReadAll(req.Body)

	if err != nil {
		panic(err)
	}

	var reqPayload models.Task
	err = json.Unmarshal(jsn, &reqPayload)

	dbConnection := rc.session.DB("hackdose").C("tasks")
	taskToUpdate := bson.M{"_id": bson.ObjectIdHex(taskId)}

	switch whatToUpdate {
	case "duration":
		updateDuration := bson.M{"$set": bson.M{"duration": reqPayload.Duration, "status": "not_picked"}}
		err := dbConnection.Update(taskToUpdate, updateDuration)
		if err != nil {
			panic(err)
		}
	case "pick_task":
		user := params.ByName("user")
		updateTaskToPicked := bson.M{
			"$set": bson.M{
				"takenby":   user,
				"datetaken": time.Now(),
				"status":    "picked",
			},
		}

		err := dbConnection.Update(taskToUpdate, updateTaskToPicked)
		if err != nil {
			panic(err)
		}
	case "task_complete":
		user := params.ByName("user")
		updateTaskToCompleted := bson.M{
			"$set": bson.M{
				"status":        "completed",
				"datecompleted": time.Now(),
			},
		}

		task := models.Task{}
		dbConnection.Find(taskToUpdate).One(&task)
		if task.TakenBy != user {
			res.Header().Set("Content-Type", "application/json")
			res.WriteHeader(http.StatusForbidden)
			res.Write([]byte(`{"error": "you were not doing this task"}`))
			return
		}

		fmt.Println(task.TakenBy, user)

		err := dbConnection.Update(taskToUpdate, updateTaskToCompleted)
		if err != nil {
			panic(err)
		}
	case "task_tested":
		user := params.ByName("user")
		updateTaskToTested := bson.M{
			"$set": bson.M{
				"status":     "tested",
				"datetested": time.Now(),
				"testedby":   user,
			},
		}

		err := dbConnection.Update(taskToUpdate, updateTaskToTested)
		if err != nil {
			panic(err)
		}
	default:
		fmt.Println("can't be")
	}

	res.WriteHeader(204)

}

func (rc *roomController) AddMoreTasks(res http.ResponseWriter, req *http.Request) {
	params := utilities.FetchParams(req)
	roomId := params.ByName("room_id")
	user := params.ByName("user")

	dbConnection := rc.session.DB("hackdose")

	jsn, err := ioutil.ReadAll(req.Body)

	var reqPayload []string
	err = json.Unmarshal(jsn, &reqPayload)

	room := models.Room{}
	err = dbConnection.C("rooms").Find(bson.M{"_id": bson.ObjectIdHex(roomId)}).One(&room)

	if err != nil {
		panic(err)
	}

	for _, task := range reqPayload {
		tempTask := models.Task{}
		tempTask.ID = bson.NewObjectId()
		tempTask.RoomId = room.ID
		tempTask.Name = task
		tempTask.Status = "new"
		tempTask.DateAdded = time.Now()
		tempTask.AddedBy = user
		err := rc.session.DB("hackdose").C("tasks").Insert(tempTask)

		if err != nil {
			panic(err)
		}
	}

	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(201)
	res.Write([]byte(`{"success": "tasks added successfully"}`))
}
