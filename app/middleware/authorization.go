package middleware

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"bitbucket.org/johnmunyui/hackdose/app/config"
	"github.com/julienschmidt/httprouter"
	"github.com/justinas/alice"
)

// Takes a path string, a handler function then returns a new handler with params set
func JWTCheker(path string, handler func(http.ResponseWriter, *http.Request)) (string, httprouter.Handle) {
	return path, wrapAuthorizeHandler(alice.New().ThenFunc(handler))
}

// Checks if Authorization header exists. Returns a http router handle function
func wrapAuthorizeHandler(handler http.Handler) httprouter.Handle {
	return func(res http.ResponseWriter, req *http.Request, ps httprouter.Params) {

		if req.Method == "GET" && req.Header.Get("Authorization") == "" && ps.ByName("token") != "" {
			tokenString := fmt.Sprintf("Token %s", ps.ByName("token"))
			req.Header.Add("Authorization", tokenString)
		}

		auth := req.Header.Get("Authorization")
		if auth == "" {
			responseError := `{"error": "please provide authentication details"}`
			res.Header().Set("Content-Type", "application/json")
			res.WriteHeader(401)
			fmt.Fprintf(res, "%s \n", responseError)
			return
		}

		token := strings.Split(req.Header.Get("Authorization"), " ")[1]
		valid, user := config.ValidateToken(token)

		if !valid {
			responseError := []byte(`{"error": "invalid token"}`)
			res.Header().Set("Content-Type", "application/json")
			res.WriteHeader(401)
			res.Write(responseError)
			return
		}

		ps = append(ps, httprouter.Param{"user", user}, httprouter.Param{"token", token})
		ctx := context.WithValue(req.Context(), "params", ps)
		req = req.WithContext(ctx)
		handler.ServeHTTP(res, req)
	}
}
