package config

import (
	"fmt"
	jwt "github.com/dgrijalva/jwt-go"
	"io/ioutil"
	"os"
	"time"
)

var privateKey []byte

func init() {
	// initialize the private key used to generate token
	secretKeyFile := os.Getenv("GOPATH") + "/src/bitbucket.org/johnmunyui/hackdose/app/config/configfiles/demo.rsa"
	privateKey, _ = ioutil.ReadFile(secretKeyFile)
}

func GetToken(username string) (token string) {
	// prepare token
	prepToken := jwt.New(jwt.SigningMethodHS256)

	claims := prepToken.Claims.(jwt.MapClaims)
	claims["username"] = username
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	// generate token string
	token, _ = prepToken.SignedString(privateKey)

	return

}

func ValidateToken(tokenString string) (valid bool, user string) {
	// grab the token
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected siging method")
		}
		return privateKey, nil
	})

	if err != nil {
		return false, err.Error()
	}

	// check if the token is valid.
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		user = fmt.Sprintf("%s", claims["username"])
		valid = true
		return
	} else {
		valid = false
		user = "incorrect"
		return
	}
}
