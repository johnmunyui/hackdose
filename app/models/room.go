package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type Room struct {
	ID          bson.ObjectId `json:"id" bson:"_id"`
	Name        string        `json:"name", bson:"name"`
	Creator     string        `json:"creator", bson:"creator"`
	Description string        `json:"description, omitempty", bson:"description", omitempty`
	DateCreated time.Time     `json:"date_created", bson:"date_created"`
	Hackers     []string      `json: "hackers", bson:"hackers"`
	Tasks       []string      `json: "tasks", bson:"tasks"`
}

type Task struct {
	ID            bson.ObjectId `json:"id,omitempty" bson:"_id"`
	Name          string        `json:"name,omitempty" bson:"name"`
	AddedBy       string        `json:"added_by,omitempty", bson:"addedby"`
	Duration      int           `json:"duration,omitempty" bson:"duration"`
	Status        string        `json:"status,omitempty" bson:"status"`
	TakenBy       string        `json:"taken_by,omitempty" bson:"takenby"`
	DateTaken     time.Time     `json:"date_taken,omitempty" bson:"datetaken"`
	DateCompleted time.Time     `json:"date_completed,omitempty" bson:"datecompleted"`
	DateTested    time.Time     `json:"date_tested,omitempty" bson:"datetested"`
	TestedBy      string        `json:"tested_by",omitempty" bson:"testedby"`
	RoomId        bson.ObjectId `json:"roomid,omitempty" bson:"roomid"`
	DateAdded     time.Time     `json:"date_added,omitempty, bson:"dateadded"`
}
