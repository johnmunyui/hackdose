package models

import (
	"time"
)

type User struct {
	UserName     string    `json:"username" bson:"username"`
	Email        string    `json:"email" bson:"email"`
	PasswordHash string    `json:"-"`
	DateCreated  time.Time `json:"date_created" bson: "date_created"`
	Avatar       string    `json:"avatar" bson:"avatar"`
	IsActive     bool      `json:"is_active" bson: "is_active"`
	LastLogIn    time.Time `json:"last_login" bson: "last_login"`
}
