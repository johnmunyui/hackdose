package templates

import (
	"html/template"
	"net/http"
	"os"

	"bitbucket.org/johnmunyui/hackdose/app/models"
)

type TplData map[string]string

type ProfileData struct {
	Data        TplData
	UserDetails models.User
	Rooms       []models.Room
	Token       string
}

type templates struct {
	tpl    *template.Template
	writer http.ResponseWriter
}

func New(w http.ResponseWriter) *templates {
	goPath := os.Getenv("GOPATH")
	path := goPath + "/src/bitbucket.org/johnmunyui/hackdose/app/templates/tpls/*.html"
	tpls := template.Must(template.ParseGlob(path))
	return &templates{tpls, w}
}

func (tpls *templates) IndexTpl() {
	data := make(TplData)
	data["title"] = "home"
	data["user"] = ""
	err := tpls.tpl.ExecuteTemplate(tpls.writer, "index.html", data)

	if err != nil {
		panic(err)
	}

}

func (tpls *templates) LoginTpl() {
	data := make(TplData)
	data["title"] = "login"
	data["user"] = ""
	err := tpls.tpl.ExecuteTemplate(tpls.writer, "login.html", data)

	if err != nil {
		panic(err)
	}
}

func (tpls *templates) WelcomeTpl(user string, token string) {
	data := make(TplData)
	data["title"] = "welcome"
	data["user"] = user
	data["token"] = token
	err := tpls.tpl.ExecuteTemplate(tpls.writer, "welcome.html", data)

	if err != nil {
		panic(err)
	}
}

func (tpls *templates) CreateRoomTpl(token string, user string) {
	data := make(TplData)
	data["title"] = "create room"
	data["token"] = token
	data["user"] = user
	err := tpls.tpl.ExecuteTemplate(tpls.writer, "room_create.html", data)

	if err != nil {
		panic(err)
	}
}

func (tpls *templates) ProfileTpl(tplData ProfileData) {
	tplData.Data = make(TplData)
	tplData.Data["title"] = "profile"
	tplData.Data["user"] = tplData.UserDetails.UserName
	tplData.Data["token"] = tplData.Token
	err := tpls.tpl.ExecuteTemplate(tpls.writer, "profile.html", tplData)
	if err != nil {
		panic(err)
	}
}

func (tpls *templates) RoomTpl(user string, token string) {
	data := make(TplData)
	data["title"] = "room"
	data["user"] = user
	data["token"] = token

	err := tpls.tpl.ExecuteTemplate(tpls.writer, "room.html", data)

	if err != nil {
		panic(err)
	}
}
