$(document).ready(function(){
    var createRoom = $("#create_room");
    var url = window.location.href
    var token = url.split("/")[5]

    var createRoomUrl = "/rooms/create_room/" + token + "/"

    createRoom.attr("href", createRoomUrl)

    var uploadButton = $("#upload_avatar")
    uploadButton.on('click', function() {
    }) 

    var userNameInput = $("input[data-type='username']")
    var registerButton = $("button[data-type='register']")

    userNameInput.focusout(function() {
        var userName = $(this).val().trim()
        var url = "/user/check_username/" + userName + "/"
        $.get(url, function(response) {
            if (response.exists) {
                $(userNameInput).next("label").attr('data-error','Username already exists');
                $(userNameInput).removeClass("valid")
                $(userNameInput).addClass("invalid")
                $(registerButton).addClass("disabled")
            } else {
                $(registerButton).removeClass("disabled")
            }
        })
    })

});
